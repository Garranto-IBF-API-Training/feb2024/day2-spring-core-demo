package com.garranto.model;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Account {

    private String accountNumber;
    private String type;
    private double balance;
    private String currency;


//    @Autowired
    public Account(String accountNumber, String type, double balance, String currency) {
        System.out.println("Account is created with all values and with some initial balance");
        this.accountNumber = accountNumber;
        this.type = type;
        this.balance = balance;
        this.currency = currency;
    }

    public Account(String accountNumber, String type, String currency) {
        System.out.println("Account created with initial balance as 0");
        this.accountNumber = accountNumber;
        this.type = type;
        this.currency = currency;
    }

    public Account(){
        System.out.println("Empty account is created");
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getType() {
        return type;
    }

    public double getBalance() {
        return balance;
    }

    public String getCurrency() {
        return currency;
    }

//    public void setAccountNumber(String accountNumber) {
//        this.accountNumber = accountNumber;
//    }

    public void setType(String type) {
        this.type = type;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

//    public void setCurrency(String currency) {
//        this.currency = currency;
//    }

    @Override
    public String toString() {
        return "Account{" +
                "accountNumber='" + accountNumber + '\'' +
                ", type='" + type + '\'' +
                ", balance='" + balance + '\'' +
                ", currency='" + currency + '\'' +
                '}';
    }
}

//Encapsulation
//1. data and functions/methods (only functions that are operating on the properties) together
//2. data direct access to the properties


//we should provide control
//read/write accesss
