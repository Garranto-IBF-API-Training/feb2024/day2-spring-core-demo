package com.garranto.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.garranto")
public class BeanConfig {


//    @Bean(value = "accOne")
//    @Lazy
//    public Account accountOne(){
//        return new Account("AC00999","Current",200,"INR");
//    }
//
//    @Bean(value = "accTwo")
//    public Account accountTwo(){
//        return new Account("AC00888","CREDIT",1000,"INR");
//    }

    @Bean
    public String accountNumber(){
        return "AC00999";
    }
}
