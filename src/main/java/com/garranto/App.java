package com.garranto;

import com.garranto.config.BeanConfig;
import com.garranto.model.Account;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.beans.Beans;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

//        create  (context)ioccontainer using an xml file present in the class path
//        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

//      creating the context  using java config

        ApplicationContext context = new AnnotationConfigApplicationContext(BeanConfig.class);

        Account accountOne = context.getBean("account",Account.class);

        System.out.println(accountOne);

    }
}

//create configuration metadata (what object are to be created and managed --  bean configurations)
//instantiate a new ioc container from the configuration
//get the objects from the container


//        Account accountOne = new Account("AC10081","Savings",4000.11,"SGD");
//        Account accounTwo = new Account("AC10082","Savings","SGD");
//        System.out.println(accountOne.getAccountNumber());
//        double currentBalance = accounTwo.getBalance();
//        double newBalance = currentBalance + 1000;
//        accounTwo.setBalance(newBalance);
//        System.out.println(accounTwo.getBalance());


//Account accountOne = context.getBean("accOne",Account.class);
//        Account accountTwo = context.getBean("accTwo", Account.class);
//
//        System.out.println(accountOne.getAccountNumber()+" with "+accountOne.getBalance());
//        System.out.println(accountTwo.getAccountNumber()+" with "+accountTwo.getBalance());

